FoorumApp.controller('ShowMessageController', function($scope, $rootScope, $routeParams, Api){
  // Toteuta kontrolleri tähän
     Api.getMessage($routeParams.id).success(function(message){
        $scope.message = message;
        $scope.newReply = {
            content: ''
        };
        console.log(message)
        $scope.replyCount = message.Replies.length;
    })
    .error(function(data, status, headers, config){
      console.log('Jotain meni pieleen...');
    })
    
    $scope.addReply = function(reply, id) {
        if (message.content) {
            Api.addReply(reply, id).success(function(reply){
                reply['User'] = {username: $rootScope.userLoggedIn.username}
                $scope.message.Replies.push(reply);
                $scope.newReply = {
                    content: ''
                }
                $scope.replyCount++
            })
            .error(function(data, status, headers, config){
                console.log('Jotain meni pieleen...');
            })           
        }
        else alert('Viestistäsi puuttuu jotain vielä.')
    }
});