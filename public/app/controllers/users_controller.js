FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  
    $scope.user = {
        username: '',
        password: ''
    }
    
    $scope.passwordConfirm = '';
    
    $scope.login = function(user) {
        Api.login(user).success(function(user){    
            console.log('Kirjautuminen onnistui!');
            $location.path('/')  
        })  
        .error(function(res){
            $scope.errorMessage = res.error;  
        });       
    }
    
    $scope.register = function(user) {
        if ($scope.passwordConfirm != user.password) {
            $scope.errorMessage = "Salasanat eivät täsmää."
            return
        }
        Api.register(user).success(function(user){    
            console.log('Kirjautuminen onnistui!');
            $location.path('/')  
        })  
        .error(function(res){
            $scope.errorMessage = res.error;  
        });    
    }

});
